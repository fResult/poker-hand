import { High, Suit } from './enum';

export class Card {
    constructor(
        public high: High,
        public suit: Suit
    ) {}
}
