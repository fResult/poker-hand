import { HandPower, Highs } from "./HandPower";
import { Card } from "./Card";
import { ComparableResult, Rank } from "./enum";

export class Hand {
    constructor(public cards: Card[]) {}

    private power(): HandPower {
        const handHighs = this.cards.map(function mapCardToBeHigh(card: Card) {
            return card.high
        }) as Highs

        // FIXME: Implement how to get true Rank from 5 cards
        return new HandPower(Rank.RoyalFlush, handHighs);
    }

    public compareWith(anotherHand: Hand): ComparableResult {
        return this.power().compareWith(anotherHand.power())
    }
}
