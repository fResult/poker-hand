import { ComparableResult, High, Rank } from './enum';
import { sortNumberAscending as sortHighAscending, zip } from './utils/list-operators';

export type Highs = [High, High, High, High, High];

export class HandPower {
    constructor(
        public readonly rank: Rank,
        public readonly highs: Highs
    ) {}

    private compareHighs(anotherHighs: Highs): ComparableResult {
        const sortedHighs = this.highs.sort(sortHighAscending);
        const sortedAnotherHighs = anotherHighs.sort(sortHighAscending);

        const zipWithAnotherHighs = zip(sortedAnotherHighs)
        const highsPairs = zipWithAnotherHighs(sortedHighs);

        for (const highsPair of highsPairs) {
            const [high, anotherHigh] = highsPair;
            if (high > anotherHigh) return ComparableResult.Win;
            else if (high < anotherHigh) return ComparableResult.Lose;
        }

        return ComparableResult.Draw;
    }


    public compareWith(anotherHandPower: HandPower): ComparableResult {
        const { rank: anotherRank, highs: anotherHighs } = anotherHandPower;

        if (this.rank > anotherRank) return ComparableResult.Win;
        else if (this.rank < anotherRank) return ComparableResult.Lose;
        else return this.compareHighs(anotherHighs);
    }
}

const hand1: HandPower =  new HandPower(
    Rank.FourOfKind,
    [High.A, High.A, High.A, High.A,High._3]
)

const hand2: HandPower = new HandPower(
    Rank.FullHouse,
    [High._4, High._4, High._4, High._10, High._10]
);

const hand3: HandPower = new HandPower(
    Rank.Straight,
    [High._6, High._5, High._4, High._3, High._2]
);

const hand4: HandPower = new HandPower(
    Rank.Straight,
    [High.A, High.K, High.Q, High.J, High._9]
);
