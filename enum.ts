export enum Suit {
    Spade = "S",
    Diamond = "D",
    Heart = "H",
    Club = "C"
}

export enum ComparableResult {
    Lose = "Lose",
    Draw = "Draw",
    Win = "Win"
}

export enum Rank {
    HighCard,
    Pair,
    TwoPairs,
    ThreeOfKind,
    Straight,
    Flush,
    FullHouse,
    FourOfKind,
    StraightFlush,
    RoyalFlush
}

export enum High {
    _2 = 2,
    _3,
    _4,
    _5,
    _6,
    _7,
    _8,
    _9,
    _10,
    J,
    Q,
    K,
    A
}
