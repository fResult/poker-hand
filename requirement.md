# Poker Hand

## Design
### Data Structure for Comparison
- **Rank**
    1. Royal Flush        -- Strongest
    2. Straight Flush
    3. Four of Kind
    4. Full House
    5. Flush
    6. Straight
    7. Three of Kind
    8. Two pairs
    9. Pair
    10. High Card         -- Weakest
- **High**
    2, 3, 4, 5, 6, 7, 8, 9, 10, A, J, Q, K

### Logical steps to compare hands
- Compare Rank in order
- Compare Highs if Rank is the same

We can easily compared highs if it is sorted by significant.
