import { Card } from "./Card";
import { High, Suit } from "./enum";
import { Hand } from "./Hand";

export const royalFlushCards: Card[] = [
    new Card(High.A, Suit.Club),
    new Card(High.Q, Suit.Club),
    new Card(High.K, Suit.Club),
    new Card(High._10, Suit.Club),
    new Card(High.J, Suit.Club)
];

export const straightFlushCards: Card[] = [
    new Card(High.J, Suit.Spade),
    new Card(High._9, Suit.Spade),
    new Card(High._7, Suit.Spade),
    new Card(High._10, Suit.Spade),
    new Card(High._8, Suit.Spade)
];

export const pairCards: Card[] = [
    new Card(High._10, Suit.Club),
    new Card(High._10, Suit.Diamond),
    new Card(High.A, Suit.Club),
    new Card(High.Q, Suit.Club),
    new Card(High.J, Suit.Club)
];

export const straightFlushCards2: Card[] = [
    new Card(High._9, Suit.Heart),
    new Card(High.J, Suit.Heart),
    new Card(High._8, Suit.Heart),
    new Card(High._7, Suit.Heart),
    new Card(High._10, Suit.Heart),
]

const royalFlushHand = new Hand(royalFlushCards);
const straightFlushHand = new Hand(straightFlushCards);
const pairHand = new Hand(pairCards);
const straightFlushHand2 = new Hand(straightFlushCards2);

const result1 = royalFlushHand.compareWith(straightFlushHand);
const result2 = straightFlushHand.compareWith(pairHand);
const result3 = pairHand.compareWith(royalFlushHand);
const result4 = straightFlushHand.compareWith(straightFlushHand2);

result1 //?
result2 //?
result3 //?
result4 //?