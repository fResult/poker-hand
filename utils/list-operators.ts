export function zip<S>([y, ...ys]: S[]) {
    return function forArr<T>([x, ...xs]: T[]): [T, S][] {
        if (xs.length === 0 || ys.length === 0) return [[x, y]]
        return [[x, y], ...zip(ys)(xs) as [[T, S]]]
    }
}

export function sortNumberAscending(a: number, b: number) {
    return b - a
}
