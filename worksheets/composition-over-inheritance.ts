class Employee {
    name: string;
}

interface Approvable {
    approve(owner: Employee): void;
}

interface Persistable {
    save(): void;
}

class StandardApprover {
    constructor(private doc: Approvable) {}

    public approveDocument(owner: Employee) {
        this.doc.approve(owner);
        console.debug(`Approved document by ${owner.name}`);
    }
}

class DocumentSaver {
    constructor(private doc: Persistable) {}

    public save() {
        this.doc.save();
        console.debug("document saved");
    }
}

class Invoice implements Approvable, Persistable {
    approve(owner: Employee): void {
        new StandardApprover(this).approveDocument(owner);
    }

    save(): void {
        new DocumentSaver(this).save();
    }
}

class Receipt implements Approvable, Persistable {
    approve(owner: Employee): void {
        new StandardApprover(this).approveDocument(owner);
    }

    save(): void {
        new DocumentSaver(this).save();
    }
}

class NewDocumentWithoutApprove implements Persistable {
    save(): void {
        new DocumentSaver(this).save
    }
}

/* ============================================================== **/

class Obj {
    name: string
}

interface Flyable {
    fly(object: Obj): void
}

class FlyingObject {
    constructor(private flyableThing: Flyable) {}

    fly (object: Obj): void {
        this.flyableThing.fly(object)
    }
}
class Bird implements Flyable {
    fly(object: Obj): void {
        new FlyingObject(this).fly(object)
    }
}
class Mosquito implements Flyable {
    public fly(object: Obj) {
        new FlyingObject(this).fly(object)
    }
}
